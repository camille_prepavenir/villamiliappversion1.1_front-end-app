/* 
Add a web app manifest
Nov 5, 2018 • Updated May 28, 2020
Appears in: Progressive Web Apps
Pete LePage
Pete LePage

    Twitter
    GitHub
    Glitch
    Blog

François Beaufort
François Beaufort

    GitHub

The web app manifest is a JSON file that tells the browser about your Progressive Web App and how it should behave when installed on the user's desktop or mobile device. A typical manifest file includes the app name, the icons the app should use, and the URL that should be opened when the app is launched.

Manifest files are supported in Chrome, Edge, Firefox, UC Browser, Opera, and the Samsung browser. Safari has partial support.
Create the manifest.webmanifest file #

The manifest file can have any name, but is commonly named manifest.webmanifest and served from the root (your website's top-level directory).

{
  "short_name": "Weather",
  "name": "Weather: Do I need an umbrella?",
  "description": "Weather forecast information",
  "icons": [
    {
      "src": "/images/icons-192.png",
      "type": "image/png",
      "sizes": "192x192"
    },
    {
      "src": "/images/icons-512.png",
      "type": "image/png",
      "sizes": "512x512"
    }
  ],
  "start_url": "/?source=pwa",
  "background_color": "#3367D6",
  "display": "standalone",
  "scope": "/",
  "theme_color": "#3367D6",
  "shortcuts": [
    {
      "name": "How's weather today?",
      "short_name": "Today",
      "description": "View weather information for today",
      "url": "/today?source=pwa",
      "icons": [{ "src": "/images/today.png", "sizes": "192x192" }]
    },
    {
      "name": "How's weather tomorrow?",
      "short_name": "Tomorrow",
      "description": "View weather information for tomorrow",
      "url": "/tomorrow?source=pwa",
      "icons": [{ "src": "/images/tomorrow.png", "sizes": "192x192" }]
    }
  ]
}

Key manifest properties #
short_name and/or name #

You must provide at least the short_name or name property. If both are provided, short_name is used on the user's home screen, launcher, or other places where space may be limited. name is used when the app is installed.
icons #

When a user installs your PWA, you can define a set of icons for the browser to use on the home screen, app launcher, task switcher, splash screen, and so on.

The icons property is an array of image objects. Each object must include the src, a sizes property, and the type of image. To use maskable icons, sometimes referred to as adaptive icons on Android, you'll also need to add "purpose": "any maskable" to the icon property.

For Chrome, you must provide at least a 192x192 pixel icon, and a 512x512 pixel icon. If only those two icon sizes are provided, Chrome will automatically scale the icons to fit the device. If you'd prefer to scale your own icons, and adjust them for pixel-perfection, provide icons in increments of 48dp.
start_url #

The start_url is required and tells the browser where your application should start when it is launched, and prevents the app from starting on whatever page the user was on when they added your app to their home screen.

Your start_url should direct the user straight into your app, rather than a product landing page. Think about what the user will want to do once they open your app, and place them there.
background_color #

The background_color property is used on the splash screen when the application is first launched on mobile.
display #

You can customize what browser UI is shown when your app is launched. For example, you can hide the address bar and browser chrome. Games can even be made to launch full screen.
Property 	Use
fullscreen 	Opens the web application without any browser UI and takes up the entirety of the available display area.
standalone 	Opens the web app to look and feel like a standalone native app. The app runs in its own window, separate from the browser, and hides standard browser UI elements like the URL bar.
An example of a PWA window with standalone display.
minimal-ui 	This mode is similar to standalone, but provides the user a minimal set of UI elements for controlling navigation (such as back and reload).
An example of a PWA window with minimal-ui display.
browser 	A standard browser experience.
scope #

The scope defines the set of URLs that the browser considers to be within your app, and is used to decide when the user has left the app. The scope controls the URL structure that encompasses all the entry and exit points in your web app. Your start_url must reside within the scope.

Caution: If the user clicks a link in your app that navigates outside of the scope, the link will open and render within the existing PWA window. If you want the link to open in a browser tab, you must add target="_blank" to the <a> tag. On Android, links with target="_blank" will open in a Chrome Custom Tab.

A few other notes on scope:

    If you don't include a scope in your manifest, then the default implied scope is the directory that your web app manifest is served from.
    The scope attribute can be a relative path (../), or any higher level path (/) which would allow for an increase in coverage of navigations in your web app.
    The start_url must be in the scope.
    The start_url is relative to the path defined in the scope attribute.
    A start_url starting with / will always be the root of the origin.

theme_color #

The theme_color sets the color of the tool bar, and may be reflected in the app's preview in task switchers. The theme_color should match the meta theme color specified in your document head.
An example of a PWA window with custom theme_color.
shortcuts #

The shortcuts property is an array of app shortcut objects whose goal is to provide quick access to key tasks within your app. Each member is a dictionary that contains at least a name and a url.
Add the web app manifest to your pages #

After creating the manifest, add a <link> tag to all the pages of your Progressive Web App. For example:

<link rel="manifest" href="/manifest.json">

Gotchas!

The request for the manifest is made without credentials (even if it's on the same domain), thus if the manifest requires credentials, you must include crossorigin="use-credentials" in the manifest tag.
Test your manifest #

To verify your manifest is setup correctly, use the Manifest pane in the Application panel of Chrome DevTools.
The application panel in Chrome Devtools with the manifest tab selected.

This pane provides a human-readable version of many of your manifest's properties, and makes it easy to verify that all of the images are loading properly.
Splash screens on mobile #

When your app first launches on mobile, it can take a moment for the browser to spin up, and the initial content to begin rendering. Instead of showing a white screen that may look to the user like the app is stalled, the browser will show a splash screen until the first paint.

Chrome automatically creates the splash screen from the manifest properties, specifically:

    name
    background_color
    icons

The background_color should be the same color as the load page, to provide a smooth transition from the splash screen to your app.

Chrome will choose the icon that closely matches the device resolution for the device. Providing 192px and 512px icons is sufficient for most cases, but you can provide additional icons for pixel perfection.
 
Further reading #
Web app manifests

    See Web technology for developers
    Web app manifests

This is an experimental technology
Check the Browser compatibility table carefully before using this in production.

Web app manifests are part of a collection of web technologies called progressive web apps (PWAs), which are websites that can be installed to a device’s homescreen without an app store. Unlike regular web apps with simple homescreen links or bookmarks, PWAs can be downloaded in advance and can work offline, as well as use regular Web APIs.

The web app manifest provides information about a web application in a JSON text file, necessary for the web app to be downloaded and be presented to the user similarly to a native app (e.g., be installed on the homescreen of a device, providing users with quicker access and a richer experience). PWA manifests include its name, author, icon(s), version, description, and list of all the necessary resources (among other things).
Members

Web manifests can contain the following keys. Click on each one to link through to more information about it:

    background_color
    categories
    description
    dir
    display
    iarc_rating_id
    icons
    lang
    name
    orientation
    prefer_related_applications
    related_applications
    scope
    screenshots
    serviceworker
    short_name
    start_url
    theme_color

Example manifest

{
  "name": "HackerWeb",
  "short_name": "HackerWeb",
  "start_url": ".",
  "display": "standalone",
  "background_color": "#fff",
  "description": "A simply readable Hacker News app.",
  "icons": [{
    "src": "images/touch/homescreen48.png",
    "sizes": "48x48",
    "type": "image/png"
  }, {
    "src": "images/touch/homescreen72.png",
    "sizes": "72x72",
    "type": "image/png"
  }, {
    "src": "images/touch/homescreen96.png",
    "sizes": "96x96",
    "type": "image/png"
  }, {
    "src": "images/touch/homescreen144.png",
    "sizes": "144x144",
    "type": "image/png"
  }, {
    "src": "images/touch/homescreen168.png",
    "sizes": "168x168",
    "type": "image/png"
  }, {
    "src": "images/touch/homescreen192.png",
    "sizes": "192x192",
    "type": "image/png"
  }],
  "related_applications": [{
    "platform": "play",
    "url": "https://play.google.com/store/apps/details?id=cheeaun.hackerweb"
  }]
}

Deploying a manifest

Web app manifests are deployed in your HTML pages using a <link> element in the <head> of a document:

<link rel="manifest" href="/manifest.webmanifest">

Note: The .webmanifest extension is specified in the Media type registration section of the specification (the response of the manifest file should return Content-Type: application/manifest+json). Browsers generally support manifests with other appropriate extensions like .json (Content-Type: application/json).

Note: If the manifest requires credentials to fetch - the crossorigin attribute must be set to use-credentials, even if the manifest file is in the same origin as the current page.
Splash screens

In Chrome 47 and later, a splash screen is displayed for sites launched from a homescreen. This splashscreen is auto-generated from properties in the web app manifest, specifically:

    name
    background_color
    The icon in the icons array that is closest to 128dpi for the device.
 */