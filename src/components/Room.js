import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import { Link } from 'react-router-dom';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import PropTypes from 'prop-types';
import MyButton from '../util/MyButton';
import DeleteRoom from './DeleteRoom';
// MUI Complex Interaction
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
// Icons
import ChatIcon from '@material-ui/icons/Chat';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
// Redux Stuff
import { connect } from 'react-redux';
import { likeRoom, unlikeRoom } from '../redux/actions/dataActions';

const styles = {
    card : {
        position: 'relative',
        display: 'flex',
        marginBottom: 20
    },
    image: {
        minWidth: 200
    },
    content:{
        padding: 25,
        objectFit: 'cover' //to avoid image to be stretched
    }
};

class Room extends Component {
    likedRoom = () => {
        if(this.props.user.likes && this.props.user.likes.find(like => like.screamId === this.props.room.roomId))
        return true;
        else return false;
    };
    likeRoom = () => {
        this.props.likeRoom(this.props.room.roomId);
    }
    unlikeRoom = () => {
        this.props.unlikeRoom(this.props.room.roomId);
    }
    render() {
        daysjs.extend(relativeTime);
        const { classes, room : { body, createdAt, userImage, userHandle, roomId, likeCount, commentCount }, user: { authenticated, credentials: { handle } } } = this.props;
// destructuring i/o => const classes = this.props.classes;
        const likeButton = !authenticated ? (
            <MyButton tip='Like'>
                <Link to='/login'>
                    <FavoriteBorder color='primary'/>
                </Link>
            </MyButton>
        ) : (
            this.likedRoom() ? (
                <MyButton tip='Undo like' onClick={this.unlikeRoom}>
                    <FavoriteIcon color='primary'/>
                </MyButton>
            ) : (
                <MyButton tip='like' onClick={this.likeRoom}>
                    <FavoriteBorder color='primary'/>
                </MyButton>
            )
        )
        const deleteButton = authenticated && userHandle === handle ? (
            <DeleteRoom roomId={roomId}/>
        ) : null
        return (
                <Card className={classes.card}>
                    <CardMedia
                    image={userImage}
                    title='Profile image'className={classes.image}/>
                    <CardContent className={classes.content}>
                        <Typography variant='h5'component={Link} to={`/users/${userHandle}`} color='primary'>{userHandle}</Typography>
                        {deleteButton}
                        <Typography variant='body2' color='textSecondary'>{dayjs(createdAt).fromNow()}</Typography>
                        <Typography variant='body1'>{body}</Typography>
                        {likeButton}
                        <span>{likeCount} Likes</span>
                        <MyButton tip='comments'>
                            <ChatIcon color='primary'/>
                        </MyButton>
                        <span>{commentCount} Comment</span>
                    </CardContent>
                </Card>
        );
    }
}

Room.propTypes = {
    likeRoom: PropTypes.func.isRequired,
    unlikeRoom: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    room: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    user: state.user
})

const mapActionsToProps = {
    likeRoom,
    unlikeRoom
}

export default connect(mapStateToProps, mapActionsToProps)(withStyles(styles)(Room));