import React, { Component, Fragment } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import MyButton from '../util/MyButton';
// MUI Stuff
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
// Icons
import DeleteOutline from '@material-ui/icons/DeleteOutline';
// Redux
import { connect } from 'react-redux';
import { deleteRoom } from '../redux/actions/dataActions';

const styles = {
    deleteButton: {
        position: 'absolute', //need to give the card a position of relative so this position on the button could work
        left: '90%',
        top: '10%'
    }
}

class DeleteRoom extends Component {
    state = {
        open: false
    };
    handleOpen = () => {
        this.setState({ open: true });
    }
    handleClose = () => {
        this.setState({ open: false });
    }
    deleteRoom = () => {
        this.props.deleteRoom(this.props.roomId);
        this.setState({ open: false });
    }
    render() {
        const { classes } = this.props;
        return (
            <Fragment>
                <MyButton tip='Delete Room' onClick={this.handleOpen} btnClassName={classes.deleteButton}>
                    <DeleteOutline color='secondary'/>
                </MyButton>
                <Dialog open={this.state.open} onClose={this.handleClose} fullWidth maxWidth='sm'>
                    <DialogTitle>
                        Are you sure you want to delete this room ?
                    </DialogTitle>
                    <DialogActions>
                        <Button onClick={this.handleClose} color='primary'>
                            Cancel
                        </Button>
                        <Button onClick={this.deleteRoom} color='secondary'>
                            Delete
                        </Button>
                    </DialogActions>
                </Dialog>

</Fragment>
        )
    }
}

DeleteRoom.propTypes = {
    deleteRoom: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    roomId: PropTypes.string.isRequired
}

export default connect(null, {deleteRoom})(withStyles(styles)(DeleteRoom));