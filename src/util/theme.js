export default {
    palette: {
        primary: {
          light: '#af52bf',
          main: '#9c27b0',
          dark: '#6d1b7b',
          contrastText: '#fff'
        },
        secondary: {
          light: '#33cbb7',
          main: '#00bfa5',
          dark: '#008573',
          contrastText: '#fff'
        },
      }, 
      typography: {
        useNextVariants: true
      },
      form: {
        texAlign: 'center'
    },
    image: {
        margin: '10px auto'
    },
    pageTitle: {
        margin: '10px auto'
    },
    textField: {
        margin: '10px auto'
    },
    button: {
        marginTop: 20,
        position: 'relative'
    },
    customError: {
        color: 'red',
        fontSize: '0.8rem',
        marginTop: 10
    },
    progress: {
        position: 'absolute' //to put the spinner in the middle we gave the btn position relative
    }
};