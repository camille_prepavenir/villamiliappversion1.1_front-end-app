import React, { Component } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import AppIcon from '../images/favicon.png';
import {Link} from 'react-router-dom';

// MUI 
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

// Redux
import { connect } from 'react-redux';
import { signupUser } from '../redux/actions/userActions';

const styles = (theme) => ({
    ...themeMV
});


// I want the form to be in the middle, so I only use the middle of 3 grid
export class signup extends Component {
    constructor(){
        super();
        this.state = {
            email: '',
            password: '',
            confirmPwd: '',
            handle: '',
            errors: {}
        }
    }
 //coz the errors are not showned, we need to get the errors and set them to our local errors
 componentWillReceiveProps(nextProps){
    //we need to put a check (w/ the if statemenent) coz we'll always be receiving props
     if (nextProps.UI.errors){ //if we get this
         this.setState({ errors: nextProps.UI.errors });//we set it to errors object
     } 
 }
    handleSubmit = (event) => {
        event.preventDefault();
        //implementation of the loading spiner as well
        this.setState({
            loading: true, //for using a spiner while connecting to server
        });
        const newUserData ={
            email: this.state.email,
            password: this.state.password,
            confirmPwd: this.state.confirmPwd,
            handle: this.state.handle
        };
        this.props.signupUser(newUserData, this.props.history);
    };
    handleChange = (event) => {
            this.setState({
                [event.target.name]: event.target.value
            });
    }
 
    render() {
        const { classes, UI: { loading } } = this.props;
        const { errors } = this.state;

        return (
            //without specifing, by default 3items will share the same space(33% of the page)
            <Grid container className={classes.form}>
                <Grid item sm/>
                <Grid item sm>
                    <img src={AppIcon} alt='VM logo' className={classes.image}/>
                    <Typography variant='h2' className={classes.pageTitle}>
                    </Typography>
                    <form noValidate onSubmit={this.handleSubmit}>
                        <TextField id='email' 
                            name='email' 
                            type='email' 
                            label='Email' 
                            className={classes.textField}
                            helperText={errors.email} //explanation of the error displayed below the field
                            error={errors.email ? true: false}
                            value={this.state.email} 
                            onChange={this.handleChange} 
                            fullWidth/>
                        <TextField id='password' 
                            name='password' 
                            type='password' 
                            label='Password' 
                            className={classes.textField}
                            helperText={errors.password} //explanation of the error displayed below the field
                            error={errors.password ? true: false}
                            value={this.state.password} 
                            onChange={this.handleChange} 
                            fullWidth/>
                            <TextField id='confirmPwd' 
                            name='confirmPwd' 
                            type='password' 
                            label='Confirm Password' 
                            className={classes.textField}
                            helperText={errors.confirmPwd} //explanation of the error displayed below the field
                            error={errors.confirmPwd ? true: false}
                            value={this.state.confirmPwd} 
                            onChange={this.handleChange} 
                            fullWidth/>
                            <TextField id='handle' 
                            name='handle' 
                            type='text' 
                            label='Handle' 
                            className={classes.textField}
                            helperText={errors.handle} //explanation of the error displayed below the field
                            error={errors.handle ? true: false}
                            value={this.state.handle} 
                            onChange={this.handleChange} 
                            fullWidth/>
                            {errors.general && (
                                <Typography variant='body 2' className={classes.customErro}>
                                    {errors.general}
                                </Typography>
                            )}
                        <Button type='submit' 
                            variant='contained' 
                            color='primary' 
                            className={classes.button}
                            disabled={loading}
                        >
                            Register
                            {loading && (
                                <CircularProgress 
                                    size={30}
                                    className={classes.progress}/>
                            )}
                        </Button>
                        <br />
                        <small>
                            Already have an account ? Login <Link to='/login'>here.</Link>
                        </small>
                    </form>
                </Grid>
                <Grid item sm/>
            </Grid>
        );
    }
}

signup.propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    UI: PropTypes.object.isRequired,
    signupUser: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    user: state.user,
    UI: state.UI
});

export default connect(mapStateToProps, { signupUser })(withStyles(styles)(signup));
