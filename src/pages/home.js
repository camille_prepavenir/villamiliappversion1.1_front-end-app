import React, { Component } from 'react';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';

import Room from '../components/Room';
import Profile from '../components/Profile';

import { connect } from 'react-redux';
import { getRooms } from '../redux/actions/dataActions';

export class home extends Component {   
    componentDidMount() {
        this.props.getRooms();
      }
    render() {
        const { rooms, loading } = this.props.data;
        let recentRoomMarkup =  !loading ? (
            rooms.map((room) => <Room key={room.roomId} room={room}/> )
        ) : (
        <p>Loading...</p>
        );
        return (
            <Grid container spacing={4}>
               <Grid item sm={8} xs={12}>
                    {recentRoomMarkup}  
               </Grid>
                <Grid item sm={4} xs={12}>
                    <Profile/>
                </Grid>
            </Grid>
        );
    }
}

home.propTypes = {
    getRooms: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    data: state.data
})

export default connect(mapStateToProps, {getRooms})(home);