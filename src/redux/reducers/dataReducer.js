// for all actions related to our data
import { SET_ROOMS, SET_ROOM, LIKE_ROOM, UNLIKE_ROOM, LOADING_DATA, DELETE_ROOM } from '../types';

const initialState = {
    rooms: [], //data of all rooms
    room: {}, // data of a single room
    loading: false
};

export default function(state = initialState, action){
    switch(action.type){
        case LOADING_DATA:
            return {
                ...state,
                loading: true
            }
        case SET_ROOMS:
            return{
                ...state,
                rooms: action.payload,
                loading: false
            }
        case LIKE_ROOM:
        case UNLIKE_ROOM:
            let index = state.rooms.findIndex((room) => room.roomId === action.payload.roomId);
            state.rooms[index] = action.payload;
            return{
                ...state
            };
        case DELETE_ROOM:
            index = state.room.findIndex(room => room.roomId === action.payload);
            state.rooms.splice(index, 1);
            return {
                ...state
            };
        default:
            return state
    }
}