import { 
    SET_USER, 
    SET_ERRORS, 
    CLEAR_ERRORS, 
    LOADING_USER, 
    SET_AUTHENTICATED, 
    SET_UNAUTHENTICATED,
    LIKE_ROOM,
    UNLIKE_ROOM
} from '../types';


const initializeState = {
    authenticated: false,
    credentials: {},
    loading: false,
    likes: [],
    notifications: []
};

//function w/ default value of state. the 2nd value is the action received
export default function(state = initializeState, action ){
    // depending of the action type we are going to do sth
    switch(action.type){
        case SET_AUTHENTICATED:
            return {
                ...state,//we return the state by spreading the one we alreday have
                authenticated: true
            };
        case SET_UNAUTHENTICATED:
            return initializeState;
        case SET_USER:
            return {
                authenticated: true,
                loading: false,
                ...action.payload // to spread the initializeState to bind each element inside accordingly
             };
        case LOADING_USER:
            return {
                ...state,
                loading: true
            };
        case LIKE_ROOM:
            return {
                ...state,
                likes: [
                    ...state.likes,
                    {
                        userHandle: state.credentials.handle,
                        screamId: action.payload.roomId
                    }
                ]
            };
        case UNLIKE_ROOM:
            return {
                ...state,
                likes: state.likes.filter((like) => like.roomId !== action.payload.roomId )
            };
        default:
            return state;
    }
}