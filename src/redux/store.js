// basically, this is the container of the application state

import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import userReducer from './reducers/userReducer';
import dataReducer from './reducers/dataReducer';
import uiReducer from './reducers/uiReducer';

const initializeState = {};

const middleware = [thunk];

const reducers = combineReducers({
    //here is our actual state, and we get to name our object inside the state
    user: userReducer,
    data: dataReducer,
    UI: uiReducer
});

//creation of our store w/ 3elements including https://github.com/zalmoxisus/redux-devtools-extension 1.1 Basic store
//this latter'll be apply as a store reducer so dat we can see da data showned on dat extension 
const store = createStore(
    reducers, 
    initializeState, 
    compose(applyMiddleware(...middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
);

export default store;