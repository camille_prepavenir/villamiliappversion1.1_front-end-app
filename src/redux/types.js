// creating types which could be just strings, but in putting in variables it makes it really hard to make a mistake
// as so, a mistake in a var won't run, and will spot our mistakes (BY CONVENTION IN ALL CAPS)

// User reducer types
export const SET_AUTHENTICATED = 'SET_AUTHENTICATED';
export const SET_UNAUTHENTICATED = 'SET_UNAUTHENTICATED';
export const SET_USER = 'SET_USER';
export const LOADING_USER = 'LOADING_USER';
// UI reducer types
export const SET_ERRORS = 'SET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
export const LOADING_UI = 'LOADING_UI';
export const LOADING_DATA = 'LOADING_DATA';
// Data reducer types
export const SET_ROOMS = 'SET_ROOMS';
export const SET_ROOM = 'SET_ROOM';
export const LIKE_ROOM = 'LIKE_ROOM';
export const UNLIKE_ROOM = 'UNLIKE_ROOM';
export const DELETE_ROOM = 'DELETE_ROOM';