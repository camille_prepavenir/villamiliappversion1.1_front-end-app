import { SET_ROOMS, LOADING_DATA, LIKE_ROOM, UNLIKE_ROOM, DELETE_ROOM } from '../types';
import axios from 'axios';

// get all rooms
export const getRooms = () => dispatch => {
    dispatch({ type: LOADING_DATA });
    axios.get('/room')
        .then(res => {
            dispatch({
                type: SET_ROOMS,
                payload: res.data
            })
        })
        .catch(err => {
            dispatch({
                type: SET_ROOMS,
                payload: []
            })
        })
}

// Like a room
export const likeRoom = (roomId) => dispatch => {
    axios.get(`/room/${roomId}/like`)
        .then(res => {
            dispatch({
                type: LIKE_ROOM,
                payload: res.data
            })
        })
        .catch(err => console.log(err));
}

// Unlike a room
export const unlikeRoom = (roomId) => dispatch => {
    axios.get(`/room/${roomId}/unlike`)
        .then(res => {
            dispatch({
                type: UNLIKE_ROOM,
                payload: res.data
            })
        })
        .catch(err => console.log(err));
}

// Delete a button
export const deleteRoom = (roomId) => (dispatch) => {
    axios.delete(`/room/${roomId}`)
        .then(() => {
            dispatch({ type: DELETE_ROOM, payload: roomId})
        })
        .catch(err => console.log(err));
}