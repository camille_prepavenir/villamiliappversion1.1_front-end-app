// similar code found previously in login and signup component
import { SET_USER, SET_ERRORS, CLEAR_ERRORS, LOADING_UI, LOADING_USER, SET_UNAUTHENTICATED } from '../types';
import axios from 'axios';

// need to pass the history (as a second parameter)to the action so it can use it
export const loginUser = (userData, history) => (dispatch) => {
    dispatch({ type: LOADING_UI}); 
    //we set the action by dispatching a type and catching it by the reducer
    axios.post('/login', userData)
            .then(res => {
                setAuthorizationHeader(res.data.token);
                dispatch(getUserData());
                dispatch({ type: CLEAR_ERRORS});
                history.push('/'); //redirect to the home page even from the action
            })
            .catch(err => {
                dispatch({
                    type: SET_ERRORS,
                    payload: err.response.data
                })
            });
};

export const signupUser = (newUserData, history) => (dispatch) => {
    dispatch({ type: LOADING_UI}); 
    axios.post('/signup', newUserData) //we send the request to signup
            .then(res => {
                setAuthorizationHeader(res.data.token);
                dispatch(getUserData());//we get the user data
                dispatch({ type: CLEAR_ERRORS});//we clear the errors
                history.push('/'); //we push the home page
            })
            .catch(err => {
                dispatch({
                    type: SET_ERRORS,//we set the errors if any
                    payload: err.response.data
                })
            });
};

// to clear out user state by setting back the initializeState(cf. userReducer.js)
export const logoutUser = (dispatch) => {
    localStorage.removeItem('IdTokenFB');
    delete axios.defaults.headers.common['Authorization'];
    dispatch({ type: SET_UNAUTHENTICATED});
}

export const getUserData = () => (dispatch) => {
    dispatch({ type: LOADING_USER});
    axios.get   ('/user')
    .then(res => {
        dispatch({
            type: SET_USER,
            payload: res.data //some data(s) sent to reducer which deals w/ it(them).
        })
    })
    .catch(err => console.log(err));
};

export const uploadImage = (formData) => (dispatch) => {
    //let's call the user loading action
    dispatch({ type: LOADING_USER})
    axios.post('/user/image', formData)
        .then(() => {
            dispatch(getUserData());
        })
        .catch(err => console.log(err));
};

export const editUserDetails = (userDetails) => (dispatch) => {
    dispatch({ type: LOADING_USER });
    axios.post('/user', userDetails)
        .then( () => {
            dispatch(getUserData());
        })
        .catch(err => console.log(err));
}

//we set the token
const setAuthorizationHeader = (token) =>{
    //to store the token if page refreshed or browser closed
    const IdTokenFB = `Bearer ${token}`;
    localStorage.setItem('IdTokenFB', IdTokenFB);
    //each time we send a request w/ axios, it gonna have this value
    axios.defaults.headers.common['Authorization'] = IdTokenFB;
};